#!/bin/bash -e

sudo apt-get install -y build-essential
sudo apt-get install -y cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install -y python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
sudo apt-get install -y unzip
pip install numpy

if [ ! -d build ]; then
    mkdir build
fi
cd build

if [ ! -f opencv.zip ]; then
    wget -O opencv.zip http://downloads.sourceforge.net/project/opencvlibrary/opencv-unix/3.0.0/opencv-3.0.0.zip?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fopencvlibrary%2Ffiles%2Fopencv-unix%2F3.0.0%2F&ts=1456511164&use_mirror=netassist
fi
unzip opencv.zip
cd opencv-*

mkdir release
cd release
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D BUILD_PYTHON_SUPPORT=ON ..
make

sudo ln /dev/null /dev/raw1394
cp lib/cv2.so /vagrant/venv/lib/python2.7/site-packages/