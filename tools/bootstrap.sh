#!/bin/bash -e

sudo apt-get update

echo "Install python deps"
sudo apt-get install -y build-essential
sudo apt-get install -y cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install -y python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
sudo apt-get install -y unzip

echo "Install Redis"
sudo apt-get install -y redis-server

echo "Install Node.js"
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y git
sudo npm install -g pm2
npm install

echo "Install Python virtualenv"
sudo apt-get install -y python-virtualenv
# Use --always-copy only on windows host machine
# virtualenv --always-copy venv
virtualenv venv

source venv/bin/activate
grep "venv" ~/.bashrc || echo "source $(pwd)/venv/bin/activate" >> ~/.bashrc
pip install -U pip
pip install -U .

echo "Install OpenCV"
sudo tar -xvvf tools/cv2.tar -C /
ln -s /lib/cv2.so venv/lib/python2.7/site-packages/

echo "Install nginx"
sudo apt-get install -y nginx-full
sudo cp tools/nginx.conf /etc/nginx/sites-available/showmetheroute
sudo ln /etc/nginx/sites-available/showmetheroute /etc/nginx/sites-enabled/showmetheroute
sudo rm /etc/nginx/sites-enabled/default

echo "Done"
