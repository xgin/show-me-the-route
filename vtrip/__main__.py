import os
import sys
import json
import logging

from main import main

logging.basicConfig(stream=sys.stderr,
                    level=int(os.getenv('LOGGING_LEVEL', logging.ERROR)))


if len(sys.argv) == 2:
    main(json.load(sys.stdin), sys.argv[1])
else:
    print("Usage: <uid> in argument and <route> in stdin")
