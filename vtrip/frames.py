import math
import hashlib
import pprint

earth_radius_in_meters = 6371000

def distance_on_unit_sphere(lat1, long1, lat2, long2):

    # Convert latitude and longitude to
    # spherical coordinates in radians.
    degrees_to_radians = math.pi / 180.0

    # phi = 90 - latitude
    phi1 = (90.0 - lat1) * degrees_to_radians
    phi2 = (90.0 - lat2) * degrees_to_radians

    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians

    # Compute spherical distance from spherical coordinates.

    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta', phi')
    # cosine( arc length ) =
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length

    cos = (math.sin(phi1) * math.sin(phi2)*math.cos(theta1 - theta2) +
           math.cos(phi1) * math.cos(phi2))
    arc = math.acos( cos )

    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    return arc

def norm_vector(point_1, point_2):
    return [point_2['x'] - point_1['x'], point_2['y'] - point_1['y']]

def vectors_angle(vec_1, vec_2):
    x1 = vec_1[0]
    y1 = vec_1[1]
    x2 = vec_2[0]
    y2 = vec_2[1]

    dot = x1 * x2 + y1 * y2      # dot product
    det = x1 * y2 - y1 * x2      # determinant
    angle = math.atan2(det, dot)  # atan2(y, x) or atan2(sin, cos)
    return angle # in radians!

def radToDeg(rad):
    return rad * 180 / math.pi

def norm_heading(heading):
    return (heading + 360) % 360

def heading(vec):
    return norm_heading(radToDeg(vectors_angle(vec, [0, 1])))

def sibling_waypoints_to_frame(wp1, wp2):
    p1 = {
        'x': wp1['lng'],
        'y': wp1['lat']
    }

    p2 = {
        'x': wp2['lng'],
        'y': wp2['lat']
    }
    return {
        'point': str(wp1['lat']) + ',' + str(wp1['lng']),
        'heading': int(heading(norm_vector(p1,p2)))
    }

def split_waypoints_to_frames(waypoints, maximum_distance):
    frames = []
    for index in range(0, len(waypoints)):
        wp1 = waypoints[index]
        if index < len(waypoints) - 1:
            wp2 = waypoints[index + 1]
            newFrame = sibling_waypoints_to_frame(wp1, wp2)
            frames.append(newFrame)

            distance = distance_on_unit_sphere(wp1['lat'], wp1['lng'], wp2['lat'], wp2['lng'])
            distance_in_meters = distance * earth_radius_in_meters
            if distance_in_meters > maximum_distance:
                count = int(distance_in_meters / maximum_distance)
                for index in range(0, count):
                    lat_delta = (wp2['lat'] - wp1['lat']) / (count + 1) * (index + 1)
                    lng_delta = (wp2['lng'] - wp1['lng']) / (count + 1) * (index + 1)
                    frames.append({
                        'point': str(wp1['lat'] + lat_delta) + ',' + str(wp1['lng'] + lng_delta),
                        'heading': newFrame['heading']
                    })
        else:
            frames_count = len(frames)
            frames.append({
                'point': str(wp1['lat']) + ',' + str(wp1['lng']),
                'heading': frames[-1]['heading'] if frames_count > 0 else 0
            })

    return frames


def add_turns(frames, maximum_turn_angle):

    def angle_to_split(heading_1, heading_2):
        angle = norm_heading(heading_2 - heading_1)
        min_angle = min(angle, 360-angle)
        if angle >= 180:
            return -min_angle
        else:
            return min_angle


    def frames_to_add_count(heading_1, heading_2):
        return int(abs(angle_to_split(heading_1, heading_2)) / maximum_turn_angle)

    if len(frames) <= 1:
         return frames

    frames_with_turns = []
    for index in range(0, len(frames)-1):
        f1 = frames[index]
        f2 = frames[index+1]
        frames_with_turns.append(f1)

        additional_frames_count = frames_to_add_count(f1['heading'], f2['heading'])
        if additional_frames_count > 0:
            for new_frame_index in range(0, additional_frames_count):
                frames_with_turns.append({
                    'point': f2['point'],
                    'heading': norm_heading(int(f1['heading'] + angle_to_split(f1['heading'], f2['heading']) / (additional_frames_count + 1) * (new_frame_index + 1)))
                })

    return frames_with_turns


# waypoints - [] of dicts { lat: float, lng: float }
# maximum_turn_angle - maximum turn angle in degrees, all larger angles will be splitted into several points with lower angles
# maximum_distance - distance between two frames, in meters
def waypoints_to_frames(waypoints, maximum_turn_angle=15, maximum_distance=15):
    frames = split_waypoints_to_frames(waypoints, maximum_distance)
    frames_with_turns = add_turns(frames, maximum_turn_angle)

    return frames_with_turns


# example
#
# lng = x
# lat = y
# waypoints = [
#   {
#     "lat": 54.994780000000006,
#     "lng": 82.87150000000001
#   },
#   {
#     "lat": 54.994910000000004,
#     "lng": 82.87066
#   },
#   {
#     "lat": 54.995000000000005,
#     "lng": 82.87055000000001
#   },
#   {
#     "lat": 54.99508,
#     "lng": 82.87041
#   },
#   {
#     "lat": 54.99515,
#     "lng": 82.87024000000001
#   },
#   {
#     "lat": 54.995400000000004,
#     "lng": 82.87
#   },
#   {
#     "lat": 54.995520000000006,
#     "lng": 82.86991
#   },
#   {
#     "lat": 54.99566000000001,
#     "lng": 82.86986
#   }
# ]

# waypoints = [
#   {
#     "lat": 55.023590000000006,
#     "lng": 82.89671000000001
#   },
#   {
#     "lat": 55.023720000000004,
#     "lng": 82.89693000000001
#   },
#   {
#     "lat": 55.023770000000006,
#     "lng": 82.89711000000001
#   },
#   {
#     "lat": 55.023880000000005,
#     "lng": 82.89733000000001
#   },
#   {
#     "lat": 55.02391000000001,
#     "lng": 82.89752
#   },
#   {
#     "lat": 55.024280000000005,
#     "lng": 82.8986
#   },
#   {
#     "lat": 55.02447000000001,
#     "lng": 82.89912000000001
#   },
#   {
#     "lat": 55.024550000000005,
#     "lng": 82.89953000000001
#   },
#   {
#     "lat": 55.02456,
#     "lng": 82.89974000000001
#   },
#   {
#     "lat": 55.024550000000005,
#     "lng": 82.90010000000001
#   },
#   {
#     "lat": 55.0245,
#     "lng": 82.9004
#   },
#   {
#     "lat": 55.02441,
#     "lng": 82.90071
#   },
#   {
#     "lat": 55.02431000000001,
#     "lng": 82.90098
#   },
#   {
#     "lat": 55.02411000000001,
#     "lng": 82.90138
#   },
#   {
#     "lat": 55.02387,
#     "lng": 82.90184
#   },
#   {
#     "lat": 55.023770000000006,
#     "lng": 82.90208000000001
#   },
#   {
#     "lat": 55.02367,
#     "lng": 82.9024
#   },
#   {
#     "lat": 55.02365,
#     "lng": 82.90247000000001
#   },
#   {
#     "lat": 55.02371,
#     "lng": 82.90274000000001
#   },
#   {
#     "lat": 55.023810000000005,
#     "lng": 82.90298000000001
#   },
#   {
#     "lat": 55.02389,
#     "lng": 82.90307000000001
#   },
#   {
#     "lat": 55.02398,
#     "lng": 82.90310000000001
#   },
#   {
#     "lat": 55.02414,
#     "lng": 82.90310000000001
#   },
#   {
#     "lat": 55.02452,
#     "lng": 82.90254
#   },
#   {
#     "lat": 55.025710000000004,
#     "lng": 82.90104000000001
#   },
#   {
#     "lat": 55.025800000000004,
#     "lng": 82.90095000000001
#   },
#   {
#     "lat": 55.027210000000004,
#     "lng": 82.89945
#   },
#   {
#     "lat": 55.02732,
#     "lng": 82.89930000000001
#   },
#   {
#     "lat": 55.027480000000004,
#     "lng": 82.89901
#   },
#   {
#     "lat": 55.027660000000004,
#     "lng": 82.89844000000001
#   },
#   {
#     "lat": 55.027770000000004,
#     "lng": 82.89799000000001
#   },
#   {
#     "lat": 55.02787000000001,
#     "lng": 82.89757
#   },
#   {
#     "lat": 55.028240000000004,
#     "lng": 82.89714000000001
#   },
#   {
#     "lat": 55.028940000000006,
#     "lng": 82.89623
#   },
#   {
#     "lat": 55.02935000000001,
#     "lng": 82.89565
#   },
#   {
#     "lat": 55.029540000000004,
#     "lng": 82.89534
#   },
#   {
#     "lat": 55.02994,
#     "lng": 82.89418
#   },
#   {
#     "lat": 55.030100000000004,
#     "lng": 82.89381
#   }
# ]

# waypoints = [{"lat":55.00835000000001,"lng":82.93573},{"lat":55.00889,"lng":82.93654000000001},{"lat":55.00912,"lng":82.93652},{"lat":55.00921,"lng":82.93645000000001},{"lat":55.0093,"lng":82.93629000000001},{"lat":55.00945,"lng":82.93592000000001},{"lat":55.00961,"lng":82.93545},{"lat":55.00983000000001,"lng":82.93400000000001},{"lat":55.00997,"lng":82.93304},{"lat":55.010130000000004,"lng":82.93161},{"lat":55.01050000000001,"lng":82.93069000000001},{"lat":55.011050000000004,"lng":82.9294},{"lat":55.011300000000006,"lng":82.92874},{"lat":55.011660000000006,"lng":82.92787000000001},{"lat":55.01189,"lng":82.92733000000001},{"lat":55.01209000000001,"lng":82.92689},{"lat":55.0123,"lng":82.92647000000001},{"lat":55.01258000000001,"lng":82.92602000000001},{"lat":55.01278000000001,"lng":82.92573},{"lat":55.01296000000001,"lng":82.92549000000001},{"lat":55.01326,"lng":82.92515},{"lat":55.013580000000005,"lng":82.92482000000001},{"lat":55.01368000000001,"lng":82.92474},{"lat":55.013850000000005,"lng":82.92461},{"lat":55.0142,"lng":82.92438000000001},{"lat":55.01437000000001,"lng":82.92433000000001},{"lat":55.014950000000006,"lng":82.92407},{"lat":55.015570000000004,"lng":82.92389},{"lat":55.01606,"lng":82.92379000000001},{"lat":55.016740000000006,"lng":82.92364},{"lat":55.01738,"lng":82.92347000000001},{"lat":55.01841,"lng":82.92332},{"lat":55.018620000000006,"lng":82.92329000000001},{"lat":55.018640000000005,"lng":82.92319},{"lat":55.01869000000001,"lng":82.92302000000001},{"lat":55.01894000000001,"lng":82.92251},{"lat":55.019040000000004,"lng":82.92234},{"lat":55.01928,"lng":82.92199000000001},{"lat":55.01953,"lng":82.92168000000001},{"lat":55.019960000000005,"lng":82.92114000000001},{"lat":55.020300000000006,"lng":82.92078000000001},{"lat":55.02049,"lng":82.92055},{"lat":55.020790000000005,"lng":82.91992},{"lat":55.02134,"lng":82.91874000000001},{"lat":55.02192,"lng":82.91857},{"lat":55.02322,"lng":82.91828000000001},{"lat":55.024390000000004,"lng":82.91795},{"lat":55.02564,"lng":82.91765000000001},{"lat":55.02718,"lng":82.91720000000001},{"lat":55.02783,"lng":82.91703000000001},{"lat":55.029140000000005,"lng":82.91668000000001},{"lat":55.030550000000005,"lng":82.91629},{"lat":55.030950000000004,"lng":82.91613000000001},{"lat":55.03145000000001,"lng":82.91600000000001},{"lat":55.03197,"lng":82.91583},{"lat":55.03333000000001,"lng":82.91544},{"lat":55.034310000000005,"lng":82.91518},{"lat":55.03558,"lng":82.91478000000001},{"lat":55.03613000000001,"lng":82.91462000000001},{"lat":55.03611000000001,"lng":82.91443000000001},{"lat":55.036210000000004,"lng":82.91423},{"lat":55.03645,"lng":82.91389000000001},{"lat":55.03681,"lng":82.91340000000001},{"lat":55.037220000000005,"lng":82.91288},{"lat":55.0375,"lng":82.91257},{"lat":55.03773,"lng":82.91232000000001},{"lat":55.03779,"lng":82.91216},{"lat":55.03835,"lng":82.91156000000001},{"lat":55.039640000000006,"lng":82.91021},{"lat":55.040110000000006,"lng":82.90969000000001},{"lat":55.04034000000001,"lng":82.90945},{"lat":55.040330000000004,"lng":82.9094},{"lat":55.040310000000005,"lng":82.9093},{"lat":55.0403,"lng":82.90919000000001},{"lat":55.040310000000005,"lng":82.90908},{"lat":55.040330000000004,"lng":82.90896000000001},{"lat":55.04045000000001,"lng":82.90874000000001},{"lat":55.04055,"lng":82.90865000000001},{"lat":55.04066,"lng":82.90863},{"lat":55.04079,"lng":82.90871000000001},{"lat":55.040890000000005,"lng":82.90888000000001},{"lat":55.04090000000001,"lng":82.90893000000001},{"lat":55.04115,"lng":82.90866000000001},{"lat":55.04175000000001,"lng":82.90801},{"lat":55.043020000000006,"lng":82.90668000000001},{"lat":55.04352000000001,"lng":82.90614000000001},{"lat":55.044090000000004,"lng":82.90556000000001},{"lat":55.044630000000005,"lng":82.905},{"lat":55.044920000000005,"lng":82.9047},{"lat":55.04598000000001,"lng":82.90373000000001},{"lat":55.04596000000001,"lng":82.90367},{"lat":55.04594,"lng":82.90357},{"lat":55.04594,"lng":82.90347000000001},{"lat":55.045970000000004,"lng":82.90327},{"lat":55.04605,"lng":82.90312},{"lat":55.046170000000004,"lng":82.90304},{"lat":55.04623,"lng":82.90304},{"lat":55.046290000000006,"lng":82.90307000000001},{"lat":55.04639,"lng":82.90319000000001},{"lat":55.04643000000001,"lng":82.90328000000001},{"lat":55.047720000000005,"lng":82.90169},{"lat":55.050790000000006,"lng":82.89801000000001},{"lat":55.052310000000006,"lng":82.89618},{"lat":55.053580000000004,"lng":82.89466},{"lat":55.056180000000005,"lng":82.89156000000001},{"lat":55.056540000000005,"lng":82.89118},{"lat":55.05666000000001,"lng":82.89109},{"lat":55.05675,"lng":82.89105},{"lat":55.05693,"lng":82.891},{"lat":55.0611,"lng":82.89189},{"lat":55.06331,"lng":82.89234},{"lat":55.064150000000005,"lng":82.89252}]
#
# id = hashlib.sha224(str(waypoints)).hexdigest()
#
#
# waypoints = [
#   {
#     "lat": 54.98568,
#     "lng": 82.89302
#   },
#   {
#     "lat": 54.985400000000006,
#     "lng": 82.89256
#   },
#   {
#     "lat": 54.98536000000001,
#     "lng": 82.89262000000001
#   },
#   {
#     "lat": 54.984840000000005,
#     "lng": 82.89355
#   }
# ]
#
# pp = pprint.PrettyPrinter(depth=6)
# pp.pprint(waypoints_to_frames(waypoints))
