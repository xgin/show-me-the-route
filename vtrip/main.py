import os
import redis
import rx

from frames import waypoints_to_frames as frames
from get_image import get_image, get_image_360
from video import VideoMaker
from injector import inject
from upload import upload_to_youtube
from mail import send_mail

r = redis.StrictRedis(host=os.getenv('REDIS_PORT_6379_TCP_ADDR'),
                      port=os.getenv('REDIS_PORT_6379_TCP_PORT'))


def main(data, uid):
    is360 = data['is360'] if 'is360' in data else False
    email = data['email'] if 'email' in data else None
    title = data['summary'].encode('utf-8') if 'summary' in data else ""
    description = "Route from %s to %s%s" % (
        data['from'].encode('utf-8') if 'from' in data else "",
        data['to'].encode('utf-8') if 'to' in data else "",
        " in 3D" if is360 else "")

    points = frames(data['waypoints'])

    xs = rx.Observable.from_(points)
    # Download images
    xs = xs.map(get_image_360 if is360 else get_image)
    # Group frames in pairs (0,1)-(1,2)-(2,3)
    xs = xs.publish(lambda ob: ob.zip(ob.skip(1), lambda x, y: (x, y)))
    with VideoMaker(uid, is360) as video_maker:
        xs = xs.map(track_progress(uid, len(points)))
        xs.subscribe(video_maker)
        result = video_maker.video_file

    if is360:
        # Inject metadata for 360 video
        inject(result)

    r.set(uid, 'u_100')
    link = upload_to_youtube(result, title, description) \
        if os.getenv('DRY_RUN') != '1' else 'LFpV5S4Xsr0'
    r.set(uid, 'y_' + link)

    if os.path.exists(result):
        os.unlink(result)

    if email:
        send_mail(email, 'https://www.youtube.com/watch?v=' + link)


def track_progress(uid, total):
    def _track(x, i):
        progress = str(int(float(float(i + 1) / total) * 100))
        r.set(uid, 'v_' + progress)
        return x
    return _track
