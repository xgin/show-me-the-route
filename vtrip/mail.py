import smtplib
from consts import *

def send_mail(to, link):
    smtpserver = smtplib.SMTP(SMTP_HOST)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.login(SMTP_LOGIN, SMTP_PASSWORD)
    from_mail = 'no-reply@' + BASE_URL
    header = 'To:' + to + '\n' + 'From: ' + from_mail + '\n' + 'Subject: Your video on youtube\n'
    header += 'MIME-Version: 1.0\r\n'
    header += 'Content-Type: text/html; charset=ISO-8859-1\r\n'
    html = '' \
           '<div>' \
           '<p style="font-size: 25px;">Your video is done and avaible on <img src="https://www.youtube.com/yt/img/logo_1x.png" style="margin-bottom: -8px;"> </p>' \
           '<a style="font-size: 25px;color:#337ab7;"href="' + link + '">' + link + '</a>' \
                                '</div>' \
                                '<p style="font-size:20px;">Create new routes on <a style="font-size:20px;color:#337ab7;"href="http://showmetheroute.com/">ShowMeTheRoute.com</a></p>'
    msg = header + '\n' + html
    smtpserver.sendmail(from_mail, to, msg)
    smtpserver.close()
