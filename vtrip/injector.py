import sys
import os
import logging

from spatialmedia import metadata_utils

logger = logging.getLogger(__name__)


def inject(input_path):
    metadata = metadata_utils.Metadata()
    metadata.video = metadata_utils.generate_spherical_xml()
    if not metadata.video:
        logger.error("Failed to generate metadata.")
        return

    output_path = input_path + '.out'
    metadata_utils.inject_metadata(input_path, output_path, metadata, logger.debug)
    os.unlink(input_path)
    os.rename(output_path, input_path)
