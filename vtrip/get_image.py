import cv2
import requests
import numpy as np

from consts import *

session = requests.Session()


def get_image_360(point):
    image = {}
    for add_heading in [240, 0, 120]:
        add_point = {
            'point': point['point'],
            'heading': int(point['heading']) + add_heading
        }

        if add_point['heading'] >= 360:
            add_point['heading'] -= 360

        add_image = get_image(add_point, fov=120)
        if add_heading == 240:
            image = add_image
        else:
            image = np.concatenate((image, add_image), axis=1)

    blank_image = np.zeros((IMAGE_HEIGHT/2, IMAGE_WIDTH * 3, 3), np.uint8)

    image = np.concatenate((blank_image, image, blank_image), axis=0)

    return image


def get_image(point, fov=None):
    query = {
        'size': IMAGE_SIZE,
        'location': point['point'],
        'heading': point['heading'],
        'pitch': PITCH,
        'key': API_KEY,
    }

    if fov:
        query['fov'] = fov

    result = session.get(REQUEST_URL, params=query)

    img_array = np.asarray(bytearray(result.content), dtype=np.uint8)

    return cv2.imdecode(img_array, cv2.IMREAD_COLOR)
