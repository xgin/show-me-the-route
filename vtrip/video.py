import cv2
import os
import rx
import logging

from consts import *

logger = logging.getLogger(__name__)


class VideoMaker(rx.Observer):
    def __init__(self, uid, is360=False):
        self.video_file = os.path.join(VIDEO_DIR, uid + '.mp4')
        width = IMAGE_WIDTH * 3 if is360 else IMAGE_WIDTH
        height = IMAGE_HEIGHT * 2 if is360 else IMAGE_HEIGHT
        self.frame_size = (width, height)
        super(VideoMaker, self).__init__()

    def __enter__(self):
        logger.debug("Video maker create: %s" % self.video_file)
        fourcc = cv2.VideoWriter_fourcc(*'H264')
        self.video = cv2.VideoWriter(
            self.video_file, fourcc, VIDEO_FPS, self.frame_size)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.video.release()

    def on_next(self, frames):
        first, second = frames
        for j in range(13):
            delta = float(j) / 13.0
            frame = cv2.addWeighted(first, 1.0 - delta, second, delta, 0)
            self.video.write(frame)

    def on_error(self, e):
        logger.error("Video maker got error: %s" % e)

    def on_completed(self):
        logger.debug("Video sequence completed: %s" % self.video_file)
