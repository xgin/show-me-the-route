FROM ekazakov/python-opencv:py2.7

RUN curl -sL https://deb.nodesource.com/setup_4.x | bash - \
    && apt-get install -y nodejs \
    && apt-get clean

RUN mkdir -p /opt/vtrip
WORKDIR /opt/vtrip

COPY requirements.txt /opt/vtrip/
RUN pip install --no-cache-dir -r requirements.txt

COPY package.json /opt/vtrip/
RUN npm install

COPY . /opt/vtrip

# CRITICAL = 50, ERROR = 40, DEBUG = 10
ENV LOGGING_LEVEL 40

# Testing should not upload video to youtube
ENV DRY_RUN 0

EXPOSE 3000

CMD [ "nodejs", "index.js" ]
