var currentRoute;
var autocompleteFrom;
var autocompleteTo;
var latLngsRoute;
var summary;
var directionsService;
var directionsDisplay;
var fetchTimer;
var map;

// Assigned either route creation or by direct opening of the page which
// looks something like this http://localhost:3000/123123146267272e324a32234234
var videoId;

var API_URL = '/api/video';
var MAIN_SITE_URL = '/';
var DISTANCE_LIMIT = 15000;

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

function clearDirectionOnMap() {
    var fromAddress = document.getElementById('address-from').value;
    var toAddress = document.getElementById('address-to').value;

    if (!fromAddress && !toAddress) {
        directionsDisplay.setMap(null);
        enableVideoCreation();
    }
}

function clearAddressFrom() {
    document.getElementById('address-from').value = '';
    if (pathEndPoints.origin) {
        pathEndPoints.origin.setMap(null);
        pathEndPoints.origin = null;
    }
    clearDirectionOnMap();
}

function clearAddressTo() {
    document.getElementById('address-to').value = '';
    if (pathEndPoints.destination) {
        pathEndPoints.destination.setMap(null);
        pathEndPoints.destination = null;
    }
    clearDirectionOnMap();
}

function addressChangeHandler() {
    calculateAndDisplayRoute(directionsService, directionsDisplay);
}

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocompleteFrom = new google.maps.places.Autocomplete(
        (document.getElementById('address-from')),
        {types: ['geocode']}
    );

    autocompleteTo = new google.maps.places.Autocomplete(
        (document.getElementById('address-to')),
        {types: ['geocode']}
    );
}

function showRouteOnYoutube(youtubeId) {
    var html = '<div class="embed-responsive embed-responsive-4by3">' +
               '<iframe class="embed-responsive-item" id="ytplayer" type="text/html" width="640" height="390"' +
               '  src="http://www.youtube.com/embed/' + youtubeId + '?autoplay=1">' +
               '</iframe>' +
               '</div>';
    document.querySelector('.diagram').style.display = 'none';
    document.querySelector('.youtube-route').innerHTML = html;
}

function updateProgress(videoStatus) {
    var type = videoStatus.split('_')[0];
    var value = videoStatus.slice(2);
    
    switch (type) {
        case 'v':
            document.querySelector('.progress-value').innerHTML = value + '%';
            document.querySelector('.progress-desc').innerHTML = 'Creating Video...';
            break;
        case 'u':
            document.querySelector('.progress-value').innerHTML = value + '%';
            document.querySelector('.progress-desc').innerHTML = 'Uploading on youtube...';
            break;
        case 'y':
            updateVideoUrl('http://youtube.com/watch?v=' + value);
            showRouteOnYoutube(value);
            clearInterval(fetchTimer);
            break;
        default:
            // do nothing
            break;
    }
}

function updateVideoUrl(url) {
    document.querySelector('.url-placeholder').innerHTML = 'Current video URL: ' + 
        '<a href="' + url + '">' + location.href + "</a>"
}

function fetchProgressData() {
    fetchTimer = setInterval(function() {
        var oReq = new XMLHttpRequest();

        oReq.open('GET', API_URL + '?id=' + videoId);
        oReq.send();

        oReq.onload = function(e) {
            if (oReq.responseText != 'false') {
                updateVideoUrl(MAIN_SITE_URL + videoId);
                updateProgress(oReq.responseText);
            } else {
                clearInterval(fetchTimer);
                showPage('notFound');
            }
        };

        oReq.onerror = function() {
            clearInterval(fetchTimer);
            console.log('Server returned ' + oReq.status)
        }  
    }, 1000);
}

function showVideoPage() {
    // switch view
    document.querySelector('.main-page').style.display = 'none';
    document.querySelector('.video-page').style.display = 'block';

    // update menu
    switchMenu();

    history.pushState({}, 'Show video page', '/' + videoId);

    fetchProgressData();
}

function startVideoCreation() {
    // if there is no some routes we shouldn't send anything to server
    if (!latLngsRoute || grecaptcha.getResponse().length == 0) {
        return;
    }

    var payload = {};
    var is360 = document.getElementById('is360').checked;

    // prepare request for server
    payload.is360 = is360;
    payload.waypoints = latLngsRoute;
    payload.from = document.getElementById('address-from').value;
    payload.to = document.getElementById('address-to').value;
    payload.summary = summary;
    payload.email = false;
    payload.g = grecaptcha.getResponse();
    if (document.getElementById('send-email').checked) {
        payload.email = document.getElementById('email').value;
    }

    var oReq = new XMLHttpRequest();

    oReq.open('POST', API_URL);
    oReq.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    oReq.send(JSON.stringify(payload));

    oReq.onload = function(e) {
        videoId = oReq.response;
        showVideoPage();
    };

    oReq.onerror = function() {
        console.log('Server returned ' + oReq.status)
    }
}

function disableVideoCreation(distance) {
    var btn = document.getElementById('video-creation');
    var msgBlock = document.querySelector('.info-message');
    var distanceKm = distance / 1000;
    btn.setAttribute('disabled', '');
    msgBlock.classList.add('_shown');
    msgBlock.innerHTML = 'The distance must be less than 15 kilometers. ' +
                         'The current distance is ' + distanceKm + ' kilometers';
}

function enableVideoCreation() {
    var btn = document.getElementById('video-creation');
    var msg = document.querySelector('.info-message');
    btn.removeAttribute('disabled');
    msg.classList.remove('_shown');
}

function checkDistance(directions) {
    var legs = directions.routes[0].legs;
    var distance = legs.reduce(function(distance, leg) {
        return distance + leg.distance.value
    }, 0);

    if (distance > DISTANCE_LIMIT) {
        disableVideoCreation(distance);
    } else {
        enableVideoCreation();
    }
}

function invalidateFields(directions) {
    var fromAddress = document.getElementById('address-from').value;
    var toAddress = document.getElementById('address-to').value;
    var requestFromAddress;
    var requestToAddress;
    var request = directions.request;

    // origin and distanation can be eather object or string
    if (typeof request.origin == 'object') {
        requestFromAddress = getLatLngString(request.origin);
    } else {
        requestFromAddress = request.origin;
    }

    if (typeof request.destination == 'object') {
        requestToAddress = getLatLngString(request.destination);
    } else {
        requestToAddress = request.destination;
    }

    if (requestFromAddress != fromAddress) {
        document.getElementById('address-from').value = requestFromAddress;
    }

    if (requestToAddress != toAddress) {
        document.getElementById('address-to').value = requestToAddress;
    }
}

function invalidateRouteData(directions) {
    latLngsRoute = directions.routes[0].overview_path.map(function(p) {
        return p.toJSON()
    });
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: {lat: 54.980082, lng: 82.890542}
    });

    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({
        draggable: true
    });

    directionsDisplay.setMap(map);

    directionsDisplay.addListener('directions_changed', function() {
        invalidateFields(directionsDisplay.getDirections());
        invalidateRouteData(directionsDisplay.getDirections());
        checkDistance(directionsDisplay.getDirections());
    });

    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
    });
}

var pathEndPoints = {};

function getLatLngString(location) {
    return location.lat() + ',' + location.lng();
}

function placeMarker(location) {
    var fromField = document.getElementById('address-from');
    var toField = document.getElementById('address-to');

    var marker = new google.maps.Marker({
        position: location, 
        map: map
    });

    if (!pathEndPoints.origin) {
        pathEndPoints.origin = marker;
        fromField.value = getLatLngString(location);
    } else if (!pathEndPoints.destination) {
        pathEndPoints.destination = marker;
        toField.value = getLatLngString(location);
    } else {
        // if there are both points then delete last
        pathEndPoints.destination.setMap(null);
        pathEndPoints.destination = marker;
        toField.value = getLatLngString(location);
    }

    if (pathEndPoints.origin && pathEndPoints.destination) {
        pathEndPoints.origin.setMap(null);
        pathEndPoints.destination.setMap(null);
    }
    addressChangeHandler();
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    var fromAddress = document.getElementById('address-from').value;
    var toAddress = document.getElementById('address-to').value;

    if (!fromAddress || !toAddress) {
        return;
    }

    directionsService.route({
        origin: fromAddress,
        destination: toAddress,
        travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
        latLngsRoute = response.routes[0].overview_path.map(function(p) {
            return p.toJSON()
        });
        summary = response.routes[0].summary
        if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setMap(map);
            directionsDisplay.setDirections(response);
            checkDistance(directionsDisplay.getDirections());
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

function initializeMainPage() {
    showPage('home');

    document.getElementById('send-email').onclick = function () {
        var isSend = document.getElementById('send-email').checked;
        if (isSend) {
            document.getElementById('email').removeAttribute(('disabled'));
        }
        else {
            document.getElementById('email').setAttribute('disabled', !isSend );
        }
    };

    /// map initialization
    initMap();

    // autocomplete initialization
    initAutocomplete();

    var startVideoCreationDebounced = debounce(startVideoCreation, 200);
    // bind handlers
    autocompleteFrom.addListener('place_changed', addressChangeHandler);
    autocompleteTo.addListener('place_changed', addressChangeHandler);
    document.getElementById('video-creation').addEventListener('click', startVideoCreationDebounced);
    document.getElementById('search-clear-to').addEventListener('click', clearAddressTo);
    document.getElementById('search-clear-from').addEventListener('click', clearAddressFrom);
    document.querySelector('.route-form').addEventListener('submit', function(e) {
        e.preventDefault();
        return false
    });
    document.querySelector('.route-form').addEventListener('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    // Methods
    // 10.54.5.120:3000/api/video
    // 10.54.5.120:3000/api/video?id=md5hash
}

function switchMenu(item) {
    var element;
    document.querySelector('.item-home').classList.remove('active');
    document.querySelector('.item-about').classList.remove('active');

    element = document.querySelector('.item-' + item);
    if (element) {
        element.classList.add('active');
    }
}

function showPage(page) {
    switch (page) {
        case 'home':
            document.querySelector('.main-page').style.display = 'block';
            document.querySelector('.video-page').style.display = 'none';
            document.querySelector('.about-page').style.display = 'none';
            document.querySelector('.notFound-page').style.display = 'none';
            initMap();
            initAutocomplete();
            history.pushState({}, 'Show main page', '/');
            break;
        case 'video':
            document.querySelector('.main-page').style.display = 'none';
            document.querySelector('.video-page').style.display = 'block';
            document.querySelector('.about-page').style.display = 'none';
            document.querySelector('.notFound-page').style.display = 'none';
            break;
        case 'about':
            document.querySelector('.main-page').style.display = 'none';
            document.querySelector('.video-page').style.display = 'none';
            document.querySelector('.about-page').style.display = 'block';
            document.querySelector('.notFound-page').style.display = 'none';
            history.pushState({}, 'Show about page', '/');
            break;
        case 'notFound':
            document.querySelector('.main-page').style.display = 'none';
            document.querySelector('.video-page').style.display = 'none';
            document.querySelector('.about-page').style.display = 'none';
            document.querySelector('.notFound-page').style.display = 'block';
            break;
        default:
            // do nothing
    }
    switchMenu(page);
}

function initializeVideoPage() {
    showPage('video');
    videoId = document.location.pathname.slice(1);
    fetchProgressData();
}

function initApp() {
    // if there is something in the pathname treat it as video page
    if (document.location.pathname != '/') {
        initializeVideoPage();
    } else {
        initializeMainPage();
    }

    // menu handlers
    document.querySelector('.item-about').addEventListener('click', function(e) {
        e.preventDefault();
        showPage('about')
    });
}
