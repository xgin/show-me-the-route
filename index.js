var redis = require('redis');
var kue = require('kue');
var express = require('express');
var md5 = require('md5');
var bodyParser = require('body-parser');
const spawn = require('child_process').spawn;
var request = require('request');
var distance = require('geo-distance');

var app = express();
const redisOptions = {
    port: process.env.REDIS_PORT_6379_TCP_PORT,
    host: process.env.REDIS_PORT_6379_TCP_ADDR
};
var redisClient = redis.createClient(redisOptions);
var queue = kue.createQueue({redis: redisOptions});
const jobConcurrency = require('os').cpus().length;

var gCaptchaSecret = '6LfPkBoTAAAAANOo0a--n_dHIqb-SXfDu_KrdlrF';

const distLimit = 15;

app.use(express.static(__dirname + '/public'));

app.get('/api/video', function (req, res) {
    var id = req.query.id;

    if (id != undefined && id.length > 0) {
        redisClient.get(id, function (err, data) {
            if (!err && data) {
                res.send(data);
            } else {
                console.log("redis error: " + err);
                res.send(false);
            }
        });
    } else {
        console.log("invalid status query");
        res.send(false);
    }
});

app.use(bodyParser.json());
app.disable('x-powered-by');

function checkDist(route, callback) {
    var d = 0;
    var from;
    var to;

    for (var i in route) {
        if (i == 0) {
            continue;
        }
        if (d > distLimit) {
            callback(route.slice(0, i));
        }
        if (i == route.length - 1) {
            callback(route);
        }

        from = {
            lon: route[i - 1].lng || 0,
            lat: route[i - 1].lat || 0
        };
        to = {
            lon: route[i].lng || 0,
            lat: route[i].lat || 0
        };

        d += distance.between(from, to) * 6371;
    }
}

function gCaptchaCheck(response, callback) {
    request.post({
      url: 'https://www.google.com/recaptcha/api/siteverify',
      form: {
          secret: gCaptchaSecret,
          response: response
      }
    }, function(error, res, body){
        var result = JSON.parse(body);

        if (result.success) {
            callback(true);
        }
        else {
            callback(false);
        }
    });
}

function checkEmpty(data) {
    return !!(data != undefined && data.length > 0 && data);
}

app.post('/api/video', function (req, res) {
    if (checkEmpty(req.body.g) && checkEmpty(req.body.waypoints)) {
        checkDist(req.body.waypoints, function(waypoints) {
            req.body.waypoints = waypoints;
            gCaptchaCheck(req.body.g, function(success) {
                if (success) {
                    var data = JSON.stringify(req.body);

                    var id = md5(JSON.stringify(req.body.waypoints));
                    redisClient.set(id, 'v_0', function () {
                        res.send(id);
                    });

                    queue.create('job-video', {
                        id: id,
                        routes: data
                    }).save();
                }
                else {
                    console.log("Error G-Captcha");
                    res.send(false);
                }
            });
        });
    }
    else {
        res.send(false);
    }
});

queue.process('job-video', jobConcurrency, function (job, done) {
    var child = spawn('python', ['-m', 'vtrip', job.data.id], {
        stdio: ['pipe', process.stdout, process.stderr]
    });
    child.stdin.write(job.data.routes);
    child.stdin.end();

    child.on('close', function (code) {
        done();
    });
});

app.use('/*', function(req, res) {
    res.sendFile('index.html', {root: './public'});
});

app.listen(3000, function () {
    console.log('App listening on port 3000!');
});
